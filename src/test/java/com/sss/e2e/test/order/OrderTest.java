package com.sss.e2e.test.order;

import com.sss.e2e.po.BillingPage;
import com.sss.e2e.po.HomePage;
import com.sss.e2e.po.catalogue.CataloguePage;
import com.sss.e2e.po.catalogue.ProductContainer;
import com.sss.e2e.test.AbstractTestBase;
import com.sss.structures.customer.Address;
import com.sss.structures.customer.Card;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import lombok.extern.java.Log;
import org.testng.annotations.Test;

import java.util.List;

import static com.codeborne.selenide.Selenide.open;

@Log
public class OrderTest extends AbstractTestBase {
    private CataloguePage cataloguePage = new CataloguePage();
    private BillingPage billingPage = new BillingPage();
    private HomePage homePage = new HomePage();

    @Test(enabled = false)
    @Description("Ku-Ku")
    public void testOne() {
        Address address = new Address();
        address.setCity("Igla");
        address.setCountry("Australia");
        address.setPostcode("433221");
        address.setStreet("Powual");
        address.setNumber("53/7");

        Card card = new Card();
        card.setLongNum("4111111111111111");
        card.setExpires("10/24");
        card.setCcv("234");

        openHomePage();
        homePage.login().login("user", "password");
        homePage.goCatalogue();
        List<ProductContainer> productList = cataloguePage.waitWhileLoadingCatalogue().products();
        productList.get(1).addToCart();
        cataloguePage
                .waitWhileLoadingCatalogue()
                .waitWhileLoading();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cataloguePage.openCart();

        billingPage.updateShippingAddress().fillInAddressForm(address);
        billingPage.updatePayment().fillInPaymentForm(card);
        billingPage.proceedToCheckout();

    }

    @Step("piu-piu")
    private void rr() {
        log.info("piu-piu");
        open("https://ya.ru");
        Allure.addAttachment("name", "content");
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshot(byte[] screenShot) {
        return screenShot;
    }
}

package com.sss.users.tests;

import lombok.extern.java.Log;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

@Log
public class UsersRegister extends UsersTestBase {

    /**
     * Test scope:
     * Test it is possible to add new user
     * <p>
     * Scenario:
     * Send POST request with new user data and expect code HTTP 200 in response
     */
    @Test
    public void testAddUserHappyPass() {
        String rndm = RandomStringUtils.random(2);
        postUsersRegister("User" + rndm, "pass" + rndm, "user" + rndm + "@ma.il", 200);
    }

    /**
     * Test scope:
     * Test it is not possible to add new user without name
     * <p>
     * Scenario:
     * Send POST request with new user data and expect code HTTP 500
     */
    @Test
    public void testAddUserWithoutUsername() {
        String rndm = RandomStringUtils.random(2);
        postUsersRegister("", "pass" + rndm, "user" + rndm + "@ma.il", 500);
    }

    /**
     * Test scope:
     * Test it is not possible to add new user without password
     * <p>
     * Scenario:
     * Send POST request with new user data and expect code HTTP 500
     */
    @Test
    public void testAddUserWithoutPassword() {
        String rndm = RandomStringUtils.random(2);
        postUsersRegister("User" + rndm, "", "user" + rndm + "@ma.il", 500);
    }

    /**
     * Test scope:
     * Test it is not possible to add new user without email
     * <p>
     * Scenario:
     * Send POST request with new user data and expect code HTTP 500
     */
    @Test
    public void testAddUserWithoutEmail() {
        String rndm = RandomStringUtils.random(2);
        postUsersRegister("User" + rndm, "pass" + rndm, "", 500);
    }

    /**
     * Test scope:
     * Test it is not possible to add duplicate user
     * <p>
     * Scenario:
     * Send POST request with new user data and expect code HTTP 500
     */
    @Test
    public void testAddUserDuplicate() {
        String rndm = RandomStringUtils.random(2);
        postUsersRegister("User" + rndm, "pass" + rndm, "user" + rndm + "@ma.il", 200);
        postUsersRegister("User" + rndm, "pass" + rndm, "user" + rndm + "@ma.il", 500);
    }

    /**
     * Method to send POST request to create new user
     *
     * @param username     user name
     * @param password     user password
     * @param email        user email
     * @param expectedCode expected response http code
     */
    private void postUsersRegister(String username, String password, String email, int expectedCode) {
        createNewUser(username, password, email)
                .then()
                .statusCode(expectedCode);
    }
}

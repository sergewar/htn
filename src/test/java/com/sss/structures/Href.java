package com.sss.structures;

import lombok.Data;

@Data
public class Href {
    private String href;
}

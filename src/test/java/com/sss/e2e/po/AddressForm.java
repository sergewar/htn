package com.sss.e2e.po;

import com.sss.e2e.po.base.AbstractForm;
import com.sss.structures.customer.Address;

import static com.codeborne.selenide.Selenide.$;

public class AddressForm extends AbstractForm {
    private static final String PREFIX = "#address-modal .modal-content ";

    public void fillInAddressForm(Address address) {
        $(PREFIX + "#form-number").sendKeys(address.getNumber());
        $(PREFIX + "#form-street").sendKeys(address.getStreet());
        $(PREFIX + "#form-city").sendKeys(address.getCity());
        $(PREFIX + "#form-post-code").sendKeys(address.getPostcode());
        $(PREFIX + "#form-country").sendKeys(address.getCountry());
        $(PREFIX + "button.btn-primary").click();
    }
}

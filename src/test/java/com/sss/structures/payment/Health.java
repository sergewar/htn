package com.sss.structures.payment;

import lombok.Data;

import java.util.List;

@Data
public class Health {
    private List<InlineModel> health;
}

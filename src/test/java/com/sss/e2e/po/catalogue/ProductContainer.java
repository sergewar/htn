package com.sss.e2e.po.catalogue;

import lombok.Getter;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class ProductContainer {
    private final String id;
    private final String name;
    private final String price;

    public ProductContainer(String id, String name, String price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public void viewDetails() {
        $("div.product .text h3 a[href='detail.html?id=" + id + "']").click();
    }

    public void addToCart() {
        $("div.product .text p.buttons a.btn-primary[onclick=\"addToCart(\'" + id + "\')\"]").click();
    }

}

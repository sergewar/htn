package com.sss.structures.order;

import com.sss.structures.customer.Address;
import com.sss.structures.customer.Card;
import com.sss.structures.customer.Customer;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class Order {
    private String id;
    private String customerId;
    private Customer customer;
    private Address address;
    private Card card;
    private List<Item> item;

    private Shipment shipment;
    private String date;
    private BigDecimal total;
}

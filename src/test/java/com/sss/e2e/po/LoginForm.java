package com.sss.e2e.po;

import com.sss.e2e.po.base.AbstractForm;

import static com.codeborne.selenide.Selenide.$;

public class LoginForm extends AbstractForm {
    private static final String PREFIX = "#login-modal .modal-content ";

    public String loginMessage() {
        return $(PREFIX + "#login-message").text();
    }

    public void login(String username, String password) {
        $(PREFIX + "input#username-modal").sendKeys(username);
        $(PREFIX + "input#password-modal").setValue(password);
        $(PREFIX + "button.btn-primary").click();
    }
}

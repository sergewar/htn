package com.sss.e2e.po.catalogue;

import com.codeborne.selenide.ElementsCollection;
import com.sss.e2e.po.base.AbstractPage;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CataloguePage extends AbstractPage {

    public CataloguePage waitWhileLoadingCatalogue() {
        $$("#products div.product").shouldHave(sizeGreaterThan(0), 10000);
        return this;
    }

    public List<String> listOfFilters() {
        return $$("#filters .checkbox").snapshot().texts();
    }

    public void filter(String filterName, boolean selectFilter) {
        if (isFilterExist(filterName)) {
            $("#filter .checkbox input[value='" + filterName + "']").setSelected(selectFilter);
        }
    }

    private boolean isFilterExist(String filterName) {
        return listOfFilters().contains(filterName);
    }

    public void sortBy(SortType sortType) {
        $(By.name("sort-by")).selectOption(sortType.getDescription());
    }

    public List<ProductContainer> products() {
        List<ProductContainer> prods = new ArrayList<>();

        ElementsCollection ec = $$("#products div.product");

        ec.forEach(e -> {
            String[] unparsedId = e.$(".text h3 a").getAttribute("href").split("=");
            String name = e.$(".text h3 a").text();
            String price = e.$(".text .price").text();
            prods.add(new ProductContainer(unparsedId[1], name, price));
        });
        return prods;
    }

}

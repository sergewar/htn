package com.sss.structures.customer;

import lombok.Data;

@Data
public class Card {
    private String id;
    private String longNum;
    private String expires;
    private String ccv;
}

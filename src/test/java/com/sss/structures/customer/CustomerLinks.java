package com.sss.structures.customer;

import com.sss.structures.Href;
import lombok.Data;

@Data
public class CustomerLinks {
    private Href self;
    private Href customer;

    private Href addresses; // optional
    private Href cards; // optional
}

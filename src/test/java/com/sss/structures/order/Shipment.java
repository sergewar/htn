package com.sss.structures.order;

import lombok.Data;

@Data
public class Shipment {
    private String id;
    private String name;
}

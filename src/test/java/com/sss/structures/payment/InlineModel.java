package com.sss.structures.payment;

import com.google.gson.annotations.Expose;
import lombok.Data;

@Data
public class InlineModel {
    @Expose
    private String service;
    private String status;
    private  String time;
}

package com.sss.e2e.po;

import com.sss.e2e.po.base.AbstractPage;

import static com.codeborne.selenide.Selenide.$;

public class HomePage extends AbstractPage {

    public void goHome() {
        $("#tabIndex a").click();
    }

    public void goCatalogue() {
        $("#tabCatalogue a").click();
    }


}

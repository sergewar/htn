package com.sss.e2e.po.base;

import static com.codeborne.selenide.Selenide.$;

public class AbstractForm {
    public void close() {
        $(".modal-dialog button.close").click();
    }
}

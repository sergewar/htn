package com.sss.structures.cart;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class CartItem {
    private String id;
    private String itemId;
    private int quantity;
    private BigDecimal unitPrice;
}

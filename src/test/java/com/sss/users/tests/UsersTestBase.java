package com.sss.users.tests;

import com.google.gson.JsonObject;
import com.sss.framework.RestTestBase;
import io.restassured.response.Response;

class UsersTestBase extends RestTestBase {

    /**
     * Method to create new user without assessing request result
     *
     * @param username user name
     * @param password user password
     * @param email    user email
     * @return response from POST request
     */
    Response createNewUser(String username, String password, String email) {
        return createRestRequest("/register")
                .body(createUserJson(username, password, email).toString())
                .post();
    }

    /**
     * Method to create json object with user details to be used in body of request
     *
     * @param username user name
     * @param password user password
     * @param email    user email
     * @return JsonObject with user parameters
     */
    private JsonObject createUserJson(String username, String password, String email) {
        JsonObject userJson = new JsonObject();
        userJson.addProperty("username", username);
        userJson.addProperty("password", password);
        userJson.addProperty("email", email);
        return userJson;
    }

    /**
     * Method to create json object with card details to be used in body of request
     *
     * @param longNum card number
     * @param expires card expiration date DD/MM
     * @param ccv     card ccv code
     * @param userId  user identification number
     * @return JsonObject with card parameters
     */
    JsonObject createCardJson(String longNum, String expires, String ccv, String userId) {
        JsonObject userJson = new JsonObject();
        userJson.addProperty("longNum", longNum);
        userJson.addProperty("expires", expires);
        userJson.addProperty("ccv", ccv);
        userJson.addProperty("userID", userId);
        return userJson;
    }
}

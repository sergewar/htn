package com.sss.e2e.po.catalogue;

import lombok.Getter;

public enum SortType {
    PRICE("Price"),
    NAME("Name"),
    SALES_FIRST("Sales first");

    @Getter
    private final String description;

    SortType(String description) {
        this.description = description;
    }


}

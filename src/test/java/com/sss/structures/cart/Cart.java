package com.sss.structures.cart;

import lombok.Data;

import java.util.List;

@Data
public class Cart {
    private List<CartItem> cartItems;
}

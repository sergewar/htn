package com.sss.structures.catalogue;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CatalogItem {
    private String id;
    private String name;
    private String description;
    private String[] imageUrl;
    private BigDecimal price;
    private int count;
    private String[] tag;
}

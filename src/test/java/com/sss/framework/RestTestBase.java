package com.sss.framework;

import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class RestTestBase {

    private static final String BASE_URI = "http://ec2-54-158-214-127.compute-1.amazonaws.com";

    /**
     * Method ti initialize request specification with basic parameters
     *
     * @param endpoint base uri for all tests
     * @return request specification to be used in tests
     */
    protected RequestSpecification createRestRequest(String endpoint) {
        return given()
                .baseUri(BASE_URI + endpoint)
                .contentType(ContentType.JSON);
    }
}

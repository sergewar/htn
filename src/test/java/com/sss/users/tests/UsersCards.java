package com.sss.users.tests;

import io.restassured.http.ContentType;
import lombok.extern.java.Log;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@Log
public class UsersCards extends UsersTestBase {

    /**
     * Test scope:
     * Test it is possible to add new card
     * <p>
     * Scenario:
     * Send POST request with new card data and expect code HTTP 200 in response
     */
    @Test
    public void testUsersAddCardHappyPass() {

        log.info("Create new user for test");
        String rndm = RandomStringUtils.random(2);
        String id = createNewUser("User" + rndm, "pass" + rndm, "user" + rndm + "@ma.il")
                .jsonPath()
                .getString("id");

        log.info("Send POST request to create new card");
        String response = postUsersCards("5555555555554444", "12/21", "666", id, 200);

        log.info("Verify response body does not contain any errors");
        assertThat(response.contains("error"), is(false));
    }

    /**
     * Test scope:
     * Test it is not possible to add new card without card number
     * <p>
     * Scenario:
     * Send POST request with new card data and expect code HTTP 500 in response
     */
    @Test
    public void testAddCardWithoutCardNumber() {

        log.info("Create new user for test");
        String rndm = RandomStringUtils.random(2);
        String id = createNewUser("User" + rndm, "pass" + rndm, "user" + rndm + "@ma.il")
                .jsonPath()
                .getString("id");

        log.info("Send POST request to create new card with empty 'longNum' value");
        String response = postUsersCards("", "12/21", "666", id, 500);

        log.info("Verify response body contain error");
        assertThat(response.contains("error"), is(true));
    }

    /**
     * Test scope:
     * Test it is not possible to add new card without expiration date
     * <p>
     * Scenario:
     * Send POST request with new card data and expect code HTTP 500 in response
     */
    @Test
    public void testAddCardWithoutExpirationDate() {

        log.info("Create new user for test");
        String rndm = RandomStringUtils.random(2);
        String id = createNewUser("User" + rndm, "pass" + rndm, "user" + rndm + "@ma.il")
                .jsonPath()
                .getString("id");

        log.info("Send POST request to create new card with empty 'expires' value");
        String response = postUsersCards("5555555555554444", "", "666", id, 500);

        log.info("Verify response body contain error");
        assertThat(response.contains("error"), is(true));
    }

    /**
     * Test scope:
     * Test it is not possible to add new card without ccv number
     * <p>
     * Scenario:
     * Send POST request with new card data and expect code HTTP 500 in response
     */
    @Test
    public void testAddCardWithoutCcv() {

        log.info("Create new user for test");
        String rndm = RandomStringUtils.random(2);
        String id = createNewUser("User" + rndm, "pass" + rndm, "user" + rndm + "@ma.il")
                .jsonPath()
                .getString("id");

        log.info("Send POST request to create new card with empty 'ccv' value");
        String response = postUsersCards("5555555555554444", "12/21", "", id, 500);

        log.info("Verify response body contain error");
        assertThat(response.contains("error"), is(true));
    }

    /**
     * Test scope:
     * Test it is not possible to add new card without user id
     * <p>
     * Scenario:
     * Send POST request with new card data and expect code HTTP 500 in response
     */
    @Test
    public void testAddCardWithoutUserId() {

        log.info("Send POST request to create new card with empty 'userId' value");
        String response = postUsersCards("5555555555554444", "12/21", "666", "", 500);

        log.info("Verify response body contain error");
        assertThat(response.contains("error"), is(true));
    }

    /**
     * Method to send POST request to create new card
     *
     * @param longNum      card number
     * @param expires      card expiration date
     * @param ccv          card ccv code
     * @param userId       user identification number
     * @param expectedCode expected response http code
     * @return response fro server in string format
     */
    private String postUsersCards(String longNum, String expires, String ccv, String userId, int expectedCode) {
        return createRestRequest("/cards")
                .contentType(ContentType.JSON)
                .body(createCardJson(longNum, expires, ccv, userId).toString())
                .post()
                .then()
                .statusCode(expectedCode)
                .extract()
                .asString();
    }
}

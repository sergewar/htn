package com.sss.structures.order;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Item {
    private  String id;
    private String itemId;
    private int quantity;
    private BigDecimal unitPrice;
}

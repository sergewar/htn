package com.sss.carts.tests;

import com.sss.framework.RestTestBase;
import com.google.gson.JsonObject;
import com.sss.framework.RestTestBase;
import io.restassured.response.Response;
import org.openqa.selenium.json.Json;

public class CartsTestBase extends RestTestBase {

    /**
     * Method to add new item to cart
     *
     * @param itemId item ID
     * @return response with status code 201
     */
    Response addCartItem(String itemId) {
        return createRestRequest("/cart")
                .body(createCartItem(itemId).toString())
                .post();
    }
    /**
     * Method to get cart items
     *
     * @return JsonObject with item list with status code 200
     */
    Response getCartItems() {
        return createRestRequest("/cart").get();
    }

    /**
     * Method to create create cart item to be used in REST HTTP request
     *
     * @param itemId item ID
     * @return JsonObject with item parameters
     */
    private JsonObject createCartItem(String itemId) {
        JsonObject itemJson = new JsonObject();
        itemJson.addProperty("id", itemId);
        return itemJson;
    }

}

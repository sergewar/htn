package com.sss.e2e.po;

import com.sss.e2e.po.base.AbstractForm;

import static com.codeborne.selenide.Selenide.$;

public class RegisterForm extends AbstractForm {
    private static final String PREFIX = "#register-modal .modal-content ";

    public String registrationMessage() {
        return $(PREFIX + "#registration-message").text();
    }

    public void fillInRegistrationForm(String username,
                                       String firstName,
                                       String lastName,
                                       String email,
                                       String password) {
        $(PREFIX + "input#register-username-modal").sendKeys(username);
        $(PREFIX + "input#register-first-modal").sendKeys(firstName);
        $(PREFIX + "input#register-last-modal").sendKeys(lastName);
        $(PREFIX + "input#register-email-modal").sendKeys(email);
        $(PREFIX + "input#register-password-modal").sendKeys(password);
        $(PREFIX + "button.btn-primary").click();
    }
}

package com.sss.carts.tests;

import com.sss.structures.cart.Cart;
import com.sss.structures.cart.CartItem;
import org.testng.annotations.Test;
import lombok.extern.java.Log;
import org.apache.commons.lang3.RandomStringUtils;

public class CartsItemsTest extends CartsTestBase {
    /**
     * Test scope:
     * Test it is possible to add new item to cart
     * <p>
     * Scenario:
     * Send POST request with new item data and expect code HTTP 201 in response
     */
    @Test
    public void testAddCartItem() {
        CartItem item = new CartItem();
        item.setItemId("03fef6ac-1896-4ce8-bd69-b798f85c6e0b");
        postCartItemAdd(item.getItemId(), 201);
    }
    /**
     * Test scope:
     * Test it is possible to add multiple new items to cart
     * <p>
     * Scenario:
     * Send POST request with multiple item data and expect code HTTP 201 in response
     */
    @Test(enabled = false)
    public void testAddMultipleOfItem() {
        Cart cart = new Cart();
        CartItem item = new CartItem();
        item.setItemId("03fef6ac-1896-4ce8-bd69-b798f85c6e0b");

        cart.getCartItems().add(item);
        cart.getCartItems().add(item);
        cart.getCartItems().add(item);

        cart.getCartItems().forEach(cartItem -> postCartItemAdd(cartItem.getItemId(), 201));
    }

    /**
     * Test scope:
     * Test it is possible to get cart items
     * <p>
     * Scenario:
     * Send GET request with multiple item data and expect code HTTP 200 in response
     */

    @Test
    public void testGetCartItems() {
        getCartItems().then().statusCode(200);
    }

    private void postCartItemAdd(String itemId, int expectedCode){
        addCartItem(itemId).then().statusCode(expectedCode);
    }

}

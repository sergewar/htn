# Evolution Gaming Team 6 Test Repository

This is a collection of tests for the 2019 TAPOST Automation Hackathon at Teikums.
Made by A. Vlohs, S. Shtubei and D. Petrovs.

## Dependencies

- Docker Engine

- JDK 11

- Maven

Dependencies are described in `pom.xml`

To download project dependencies and compile project, run 
```
mvn compile
```

## Run tests

Recommended to run 
```
mvn clean package
```

To run Selenide tests inside Docker, prepare Selenoid hub by running
```
sh start-local-selenoid.sh
```

And use VM option: `-Dselenide.remote=http://localhost:4444/wd/hub`

## Allure report

Go get allure report locally, run
`allure serve`
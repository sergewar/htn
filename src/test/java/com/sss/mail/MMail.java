package com.sss.mail;

import org.simplejavamail.email.Email;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.Mailer;
import org.simplejavamail.mailer.MailerBuilder;
import org.simplejavamail.mailer.config.TransportStrategy;

import static javax.mail.Message.RecipientType.BCC;

public class MMail {
    Mailer mailer = MailerBuilder
            .withSMTPServer("smtp.host.com", 587, "user@host.com", "password")
            .withTransportStrategy(TransportStrategy.SMTP_TLS)
            .withProxy("socksproxy.host.com", 1080, "proxy user", "proxy password")
            .withSessionTimeout(10 * 1000)
            .clearEmailAddressCriteria() // turns off email validation
            .withProperty("mail.smtp.sendpartial", "true")
            .withDebugLogging(true)
            .buildMailer();

    private void dfkj() {
        // Most essentials together (but almost everything is optional):

//        ConfigLoader.loadProperties("simplejavamail.properties"); // optional default
//        ConfigLoader.loadProperties("overrides.properties"); // optional extra

        Email email = EmailBuilder.startingBlank()
                .to("lollypop", "lolly.pop@somemail.com")
                .to("C. Cane", "candycane@candyshop.org")
                .ccWithFixedName("C. Bo group", "chocobo1@candyshop.org", "chocobo2@candyshop.org")
                .withRecipientsWithFixedName("Tasting Group", BCC,
                        "taster1@cgroup.org;taster2@cgroup.org;tester <taster3@cgroup.org>")
                .bcc("Mr Sweetnose <snose@candyshop.org>")
                .withReplyTo("lollypop", "lolly.pop@othermail.com")
                .withSubject("hey")
                .withHTMLText("<img src='cid:wink1'><b>We should meet up!</b><img src='cid:wink2'>")
                .withPlainText("Please view this email in a modern email client!")
//                .withEmbeddedImage("wink1", imageByteArray, "image/png")
//                .withEmbeddedImage("wink2", imageDatesource)
//                .withAttachment("invitation", pdfByteArray, "application/pdf")
//                .withAttachment("dresscode", odfDatasource)
                .withHeader("X-Priority", 5)
                .withReturnReceiptTo()
                .withDispositionNotificationTo("notify-read-emails@candyshop.com")
                .withBounceTo("tech@candyshop.com")
//                .signWithDomainKey(privateKeyData, "somemail.com", "selector")
                .buildEmail();

        mailer.sendMail(email);
    }
}

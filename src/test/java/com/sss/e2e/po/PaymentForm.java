package com.sss.e2e.po;

import com.sss.e2e.po.base.AbstractForm;
import com.sss.structures.customer.Card;

import static com.codeborne.selenide.Selenide.$;

public class PaymentForm extends AbstractForm {
    private static final String PREFIX = "#card-modal .modal-content ";

    public void fillInPaymentForm(Card card) {
        $(PREFIX + "#form-card-number").sendKeys(card.getLongNum());
        $(PREFIX + "#form-expires").sendKeys(card.getExpires());
        $(PREFIX + "#form-ccv").sendKeys(card.getCcv());
        $(PREFIX + "button.btn-primary").click();
    }
}

package com.sss.structures.payment;

import lombok.Data;

@Data
public class PaymentAuth {
    private boolean authorised;
}

package com.sss.e2e.test;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;

public abstract class AbstractTestBase {

    @AfterClass
    public static void closeBrowser() {
        closeWebDriver();
    }

    @BeforeClass
    public void openBrowser() {
        List<String> flags = new ArrayList<>();
        flags.add("--disable-dev-shm-usage");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments(flags);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        Configuration.browserCapabilities=capabilities;
        open();
    }

    public void openHomePage() {
        open("http://ec2-54-158-214-127.compute-1.amazonaws.com/");
    }
}

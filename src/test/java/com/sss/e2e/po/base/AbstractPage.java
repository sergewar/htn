package com.sss.e2e.po.base;

import com.sss.e2e.po.LoginForm;
import com.sss.e2e.po.RegisterForm;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public abstract class AbstractPage {

    public LoginForm login() {
        $("#login a").click();
        return new LoginForm();
    }

    public void logout() {
        $("#logout a").click();
    }

    public RegisterForm register() {
        $("#logout a").click();
        return new RegisterForm();
    }

    public void openCart() {
        $("#basket-overview a").click();
    }

    public void waitWhileLoading() {
        $("#navbar").waitUntil(visible, 5000);
    }
}

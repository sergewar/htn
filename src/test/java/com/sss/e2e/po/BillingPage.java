package com.sss.e2e.po;

import com.sss.e2e.po.base.AbstractPage;

import static com.codeborne.selenide.Selenide.$;

public class BillingPage extends AbstractPage {
    public void continueShopping() {
        $(".box-footer i.a-chevron-left").click();
    }

    public void updateBasket() {
        $(".box-footer i.fa-refresh").parent().click();
    }

    public void proceedToCheckout() {
        $(".box-footer  #orderButton").click();
    }

    public AddressForm updateShippingAddress() {
        $("[data-target='#address-modal']").click();
        return new AddressForm();
    }

    public PaymentForm updatePayment() {
        $("[data-target='#card-modal']").click();
        return new PaymentForm();
    }
}

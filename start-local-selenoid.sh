#!/usr/bin/env sh

echo Pulling selenoid/chrome container
docker pull selenoid/chrome:latest

echo Run Docker container
docker run selenoid/chrome
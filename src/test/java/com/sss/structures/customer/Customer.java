package com.sss.structures.customer;

import lombok.Data;

import java.util.List;

@Data
public class Customer {
    private String id;
    private String firstName;
    private String lastName;
    private String userName;
    private List<Address> addresses;
    private List<Card> cards;

}
